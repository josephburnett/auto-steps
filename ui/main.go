package main

import (
	"fmt"
	"log"
	"sync"
	"syscall/js"

	schema "gitlab.com/josephburnett/auto-step-runner/schema/v1"
)

func main() {
	app := newUI()
	log.Print("app running")
	_, _, err := app.render("", app.step)
	if err != nil {
		log.Fatalf(err.Error())
	}
	select {}
}

type ui struct {
	mux  sync.Mutex
	doc  js.Value
	step *schema.Step
}

func newUI() *ui {
	return &ui{
		doc: js.Global().Get("document"),
	}
}

func (u *ui) getElementByID(id string) js.Value {
	return u.doc.Call("getElementById", id)
}

func (u *ui) render(path string, step *schema.Step) (string, changeFn, error) {
	if step == nil {
		out, _, err := u.renderStepSelect(path)
		if err != nil {
			return "", nil, fmt.Errorf("rendering step select: %w", err)
		}
		el := u.getElementByID("app")
		if el.IsUndefined() {
			return "", nil, fmt.Errorf("nil app")
		}
		el.Set("innerHTML", out)
		return "", nil, nil
	}
	return u.step.Step.Short, nil, nil
}

type changeFn func() error

func (u *ui) renderStepSelect(path string) (string, changeFn, error) {
	id := "select." + path
	out := `<select name="step" id="` + id + `">`
	out += `<option value="./steps/if">If</option>`
	out += `<option value="./steps/add_label">Add Label</option>`
	out += `</select>`
	return out, func() error {
		step := u.getElementByID(id).String()
		// TODO dig for the correct step
		u.mux.Lock()
		defer u.mux.Unlock()
		u.step = &schema.Step{
			Step: schema.Reference{
				Short: step,
			},
		}
		return nil
	}, nil
}
