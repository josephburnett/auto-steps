package main

import (
	"log"
	"net/http"
	"sync"
)

func main() {
	fs := http.FileServer(http.Dir("./ui/assets"))
	http.Handle("/", fs)
	log.Printf("serving on http://localhost:8080")
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal(err)
	}
}

type server struct {
	mux sync.Mutex
}

func (s *server) handle(w http.ResponseWriter, r *http.Request) {
	s.mux.Lock()
	defer s.mux.Unlock()

}
