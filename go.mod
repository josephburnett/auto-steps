module gitlab.com/josephburnett/auto-steps

go 1.21.5

require gitlab.com/josephburnett/auto-step-runner v0.0.0-20240423213919-5f828c54dbcb

require (
	github.com/bahlo/generic-list-go v0.2.0 // indirect
	github.com/buger/jsonparser v1.1.1 // indirect
	github.com/invopop/jsonschema v0.12.0 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/wk8/go-ordered-map/v2 v2.1.8 // indirect
	golang.org/x/exp v0.0.0-20230905200255-921286631fa9 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
