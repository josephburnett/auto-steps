.PHONY : serve
serve : build-ui build-server
	./build/server

.PHONY : build-server
build-server :
	go build -o build/server ./server/main.go

.PHONY : build-ui
build-ui:
	cp $$(go env GOROOT)/misc/wasm/wasm_exec.js ui/assets/
	GOOS=js GOARCH=wasm go build -o ui/assets/ui.wasm ./ui/main.go
